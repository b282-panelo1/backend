// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// Using the aggregate method
/*
SYNTAX:
  db.collectionName.aggregate([
    {$match: {field: value}},
    {$group: {_id: "$field", total: {operation}}}
  ]);
*/

// $ symbol will refer to a field name that is available in the documents that are being aggregated on

db.fruits.aggregate([
  // $match is used to pass documents that meet specified condition(s) to the next pipeline  stage/aggregation process
  {$match: {onSale: true}},

  // $group is used to group elements together and fiel-value pairs using the data from the grouped elements
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

// Field projection with aggregation
/*
SYNTAX:
  {$project: {field: 1/0}}
*/

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  {$project: {_id: 0}}
]);

// Sorting aggregated result
/*
SYNTAX:
  {$sort: {field: 1/-1}}
*/
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  {$sort: {total: 1}}
]);

// Aggregating result based on array fields
/*
SYNTAX:
  {$unwind: field}
*/

db.fruits.aggregate([
  // $unwind deconstructs an array field from a collection with an array value to output a result for each element
  {$unwind: "$origin"}
]);


// Display fruit documents by their origin and the kinds of fruit that are supplied
db.fruits.aggregate([
  {$unwind: "$origin"}, 
  {$group: {_id: "$origin", how_many_kind_of_fruits: {$sum: 1}}}
]);