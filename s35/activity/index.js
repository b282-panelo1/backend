const express = require("express");

// "mongoose" is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;

// Connecting to mongodbatlas
mongoose.connect("mongodb+srv://nicoppanelo:aRIv60YfTgWgyy1r@wdc028-course-booking.vc04om1.mongodb.net/s35",
	// Allows us to avoid any current and future errors while connecting to MongoDB
	{
		useNewURLParser: true,
		useUnifiedTopology: true
	}
);


// Connecting to MongoDB locally
// Allows to handle errors when the initial connection is established
let db = mongoose.connection;

// "console.error.bind" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, "We're connected to the cloud database!" output in the console
db.once("open", () => console.log("We're connected to the cloud database!"));


// Middleware
// read json data
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// [SECTION] Mongoose Schema
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data

// The "new" keyword create a new Schema
// Use the Schema() constructor of the Mongoose module to create a  new schema object

/*
SYNTAX:
	const schemaName = new mongoose.Schema({

})
*/

/*
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
*/

// [SECTION] Models
// Models must be in singular form and first letter is capitalized
// First parameter of the mongoose model method indicates the collection in where to store the data
// Second parameter is used to specify teh Schema bluepriny of the documents that will be stored in the MongoDB collection

/*
SYNTAX:
	const Model = mongoose.model("collectionName", schemaName);
*/

/*
const Task = mongoose.model("Task", taskSchema);
*/

/*
Creating a new task
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

/*
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}).then((result, err) => {
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate task found!");
		} else {
			let newTask = new Task ({
				name: req.body.name
			});
			// save() method will store the information to the database
			newTask.save().then((savedTask, saveErr) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New Task created!");
				} // line 92
			}) // line 89
		} // line 85
	}) // line 82
}) // line 81
*/


/*
Getting all the tasks
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

/*
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			}) // line 113
		} // line 112
	}) // line 109
}) // line 108
*/


// Activity

// User Schema

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// User Model

const User = mongoose.model("User", userSchema);

// POST route /signup

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}).then((result, err) => {
		if(result != null && result.username == req.body.username) {
			return res.send("Duplicate username found!");
		} else {
			if(req.body.username !== "" && req.body.password !== "") {
				let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save().then((savedUser, saveErr) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered");
				}
			})
			} else if (req.body.username !== "" || req.body.password !== ""){
				return res.send("Both username and password must be provided")
			}
		} 
	})
})




app.listen(port, () => console.log(`Server running at port ${port}!`))