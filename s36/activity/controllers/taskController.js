// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be places in this file

const Task = require("../models/task");

// Controller for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// Controller for creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			return false;
		} else {
			return task
		};
	});
};

// Controller deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return "Deleted task."
		};
	});
};

// Controller updating a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return "Task updated.";
			};
		});
	});
};




// Activity s36



// getSpecificTask
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
};



// changeStatus
module.exports.changeStatus = (taskId) => {
	return Task.findByIdAndUpdate(taskId, { status: "complete" }, { new:true }).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}