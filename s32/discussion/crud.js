let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

http.createServer(function(request, response) {

	// GET METHOD
	if(request.url == "/users" && request.method == "GET") {
		// Sets response output to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'})
		// write() is a method in node.js that is used to write data to the response body in HTTP server
		// JSON.stringify() method converts the string input to JSON
		response.write(JSON.stringify(directory));
		response.end()
	};

	// POST METHOD
	if(request.url == "/users" && request.method == "POST"){
		let requestBody = '';
		
			request.on('data', function(data){
				requestBody += data;
			});
		 
			request.on('end', function(){
		    requestBody = JSON.parse(requestBody);
		    
		    let newUser = {
		    	"name" : requestBody.name,
		    	"email" : requestBody.email
		    }

		    directory.push(newUser)
		    console.log(directory);

			response.writeHead(200,{'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
			});
	};


}).listen(3000);

console.log('Server running at localhost: 3000');