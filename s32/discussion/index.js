let http = require("http");

http.createServer(function(request, response) {

	// endpoint: "/items"
	// response: 'Data retrieved from the database'

	if(request.url == "/items" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database')
	}

	// endpoint: /items
	// method: POST
	// Data to be sent to the database

	if(request.url == "/items" && request.method == "POST") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database')
	}


}).listen(4000);

console.log('Server running at localhost: 4000');