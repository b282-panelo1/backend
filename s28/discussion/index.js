// CRUD Operations

// Create Operation
// insertOne() - Inserts one document to the collection
db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
});

// insertMany() - Inserts multiple documents to the collection
db.users.insertMany([
	{ "firstName": "John", "lastName": "Doe" },
	{ "firstName": "Jane", "lastName": "Doe" }
]);

// Read Operation
// find() - get all the inserted users
db.users.find();

// Retrieving specific documents
db.users.find({ "lastName": "Doe" });

// Update Operation
// updateOne() - modify one document
db.users.updateOne(
	{
		"_id": ObjectId("648af8ec34e720e614fd1fec")
	},
	{
		$set: {
			"email": "johnsmith@gmail.com"
		}
	}
);

// updateMany() - modify multiple documents
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"isAdmin": false
		}
	}
);

// Delete Operation
// deleteMany() - delete multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// deleteOne() - deletes single document
db.users.deleteOne({ "_id": ObjectId("648af8ec34e720e614fd1fec") });

/*
Hi B282!
//Create a brand new database called - "session-recap"
//After creating your database, you may now create these documents

//1. CREATE

//Create one document

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

//Insert multiple documents

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "none"
    }
]);


//2.RETRIEVE

//This will retrieve all the documents?
//db.users.find()

//Retrieve a document that has Stephen as its firstName
db.users.find({ "firstName": "Stephen" });

//Retrieve document/s with Armstrong as its lastName and 82 as its age
db.users.find({ "lastName" : "Armstrong", "age" : 82});

//3. UPDATE

//We are now going to crate a document to update
//Creating a document to update

db.users.insertOne({
    firstName: "Test",
  lastName:"Test",
  age: 0,
  contact:{
    phone: "00000000",
    email:"test@gmail.com"
  },
  courses:[],
  department:"none"
})

//Update a document with a specific criteria

db.users.updateOne(
    {firstName: "Test"},
  {
    $set : {
        firstName: "Bill",
      lastName: "Gates",
      age: 65,
      contact: {
        phone: "12345678",
        email: "bill@gmail.com"
      },
      course: ["PHP","Laravel","HTML"],
      department: "Operations",
      status: "active"
    }
  }
)

//Update multiple documents

db.users.updateMany(
    
  {department: "none"},
  {
    $set: {department: "HR"}
  }
);

//we can also replace the whole document
db.users.replaceOne(
    {firstName: "Bill"},
  {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
      email: "bill@gmail.com"
    },
    courses: ["PHP","Laravel","HTML"],
    department:"Operations"
  }
)

//4. DELETE
 
//create a document we are going to delete

db.users.insertOne({
    firstName: "test"
})

//How can we delete a single document with test as its firstname? 
db.users.deleteOne({
    firstName: "test"
})

//How can we delete multiple documenst with Bill as its firstName?

db.users.deleteMany({
    firstName: "Bill"
})

*/

//Advanced Queries

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
});

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
);

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } );

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } );

// Querying an Embedded Array
db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
});





db.users.insertMany(
{
       firstName: "Test",
      lastName:"Test",
      age: 0,
      contact:{
        phone: "00000000",
        email:"test@gmail.com"
      },
      courses:[],
      department:"HR"
},
{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    }
);