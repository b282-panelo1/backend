// Controllers contain the functions and business logic of our express application

const User = require("../models/User");
const Course = require("../models/Course")

const bcrypt = require("bcrypt");

const auth = require("../auth");


/*
Check if email already exists
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the frontend application/Postman based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


/*
Controllers for user registration
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


/*
Controllers for user authentication
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(
					result)}
			} else {
				return false;
			};
		};
	});
};


// Activity

/*
Retrieve user details
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend
*/
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};


// Controllers for user enrollment
module.exports.enroll = async (data) => {
	
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});


	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	};
};
