const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
const cors = require("cors");

const userRoute = require("./routes/userRoute");

const courseRoute = require("./routes/courseRoute");

const app = express();

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://nicoppanelo:aRIv60YfTgWgyy1r@wdc028-course-booking.vc04om1.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all resources to access our backend application
app.use(cors());

// Defines the "/users" string to be included for all user routes defined in the "userRoute" file
app.use("/users", userRoute);

// Defines the "/courses" string to be included for all user routes defined in the "courseRoute" file
app.use("/courses", courseRoute);


// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));