// console.log("Hello World!");

// [SECTION] if, else if, and else statement

// if Statement
// executes a statement if a specified condition is true

let numA = -1

if(numA < 0) { // true
	console.log("Hello!");
}

// if(numA > 0) { // false
// 	console.log("Hello!");
// }

// else if Clause
// executes a statement if previous conditions are false and if the specified condition is true

let numB = 1;

if (numA > 0) { // false
	console.log("Hello!");
} else if (numB > 0) { // true
	console.log("World!");
}

// else Statement 
// executes a statement if all other conditions are false

if (numA > 0) { // false
	console.log("Hello!");
} else if (numB == 0) { // false
	console.log("World!");
} else {
	console.log("Hello World!");
}

// windSpeed < 30 → Not a typhoon yet.
// windSpeed <= 61 → Tropical depression detected.
// windSpeed >= 62 && windSpeed <= 88 → Tropical storm detected.
// windSpeed >= 89 && windSpeed <= 117 → Severe tropical storm detected

// if, else if, and else statements with functions

let message;

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		// return statement is used within a function to specify the value that should be returnrd when the function is called
		return 'Not a typhoon'
	}
	else if(windSpeed <= 61) {
		return 'Tropical storm detected.'
	}
	else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.'
	}
	else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected'
	}
	else {
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(65);
console.warn(message);
// console.log(message);

if(message == 'Tropical storm detected') {
	// console.warn() is a good way to print warnings in our console that could help us  developers act on certain output within our code
	console.warn(message);
}

// [SECTION] Truthy and Falsy
// In JS a "truthy" value is a value that is considered true when encountered in a Boolean context
// Values are considered true unless defined otherwise

/*
Falsy values/ exceptions for truthy:
1. false
2. 0
3. -0
4. ""
5. null
6. undefined
7. NaN
*/

// Truthy samples
if(true) {
	console.log('Truthy');
}

if(1) {
	console.log('Truthy');
}

if([]) {
	console.log('Truthy');
}

// Falsy examples
if(false) {
	console.log('Falsy');
}

if(0) {
	console.log('Falsy');
}

if(undefined) {
	console.log('Falsy');
}

// [SECTION] Conditional (Ternary) Operator
// can be used as an alternative to an "if else" statement
// Ternary operators have an implicit "return" statement

/*
The conditional (ternary) operator takers in 3 operands:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy
*/

/*
SYNTAX
	(condition) ? ifTrue : ifFalse;
*/

// Single statement execution
// kung 1 conditions lang pwede gamitin
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple statement execution

let name;

function isOfLegalAge() {
	name = "John";
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = "Jane";
	return 'You are under the age limit'
}

// The "parseInt()" function converts the input received into number datatype
// let age = parseInt(prompt("What is your age?"));
// console.log(age); //10

// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// [SECTION] Switch Statement
// can be used as an alternative to an if, else if, and else atement where the data to be used in the condition is of an expected input

/*
SYNTAX:
	switch (expression) {
		case value:
		statement;
		break;
	default:
		statement;
		break;
	}
*/

// "toLowerCase()" function/method will change the input received from the prompt into all lowercase letters
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

// switch cases are considered as "loops", meaning it will compare the expression with each of the case values untill a match is found
switch (day) {
	case 'monday':
		console.log("The color of the day is red");
		// used to terminate the current loop once a match has been found
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
       	console.log("The color of the day is yellow");
       	break;
	case 'thursday':
	    console.log("The color of the day is green");
	    break;
	case 'friday':
	    console.log("The color of the day is blue");
	    break;
	case 'saturday':
	    console.log("The color of the day is indigo");
	    break;
	case 'sunday':
	    console.log("The color of the day is violet");
	    break;
	default:
		console.log("Please input a valid day!");
		break;
}


 // [SECTION] Try-Catch-Finally Satement
 // "try catch" statements are commonly used for error handling

function showInternsityAlert(windSpeed) {

	try {
		// Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	} catch (error) {

		// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is
		console.log(typeof error);
		// "error.message" - is used to access the information relating to an error object
		console.warn(error.message);
	} finally {
		// continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
		alert('Intensity updates will show new alert')
	}
	
}

showInternsityAlert(56);