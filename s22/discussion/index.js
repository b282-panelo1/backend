// console.log("Hello World!");

// Array Methods
// JS has built-in functions and methods for arrays
// This allows us to manipulate and access array items

// [SECTION] Mutator Methods
// Mutator methods are functions that mutate or change an array they are created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log("Original Array:");
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push()
// Adds an element inthe end of an array and returns the updated array's length

/*
SYNTAX:
	arrayName.push();
*/

let fruitsLength = fruits.push('Mango');
console.log("fruitsLength:")
console.log(fruitsLength); // 5
console.log("Mutated array from push() method:")
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango'];

// Pushing multiple elements to an array
fruitsLength = fruits.push('Avocado', 'Guava');
console.log("fruitsLength:")
console.log(fruitsLength); // 7
console.log("Mutated array from push() method:")
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava'];

//  pop()
// Removes the last  element and return the removed element

/*
SYNTAX:
	arrayName.pop();
*/
console.log("Current Array:");
console.log(fruits);

let removedFruit = fruits.pop();

console.log("Removed fruit:");
console.log(removedFruit); // Guava
console.log("Mutated array from pop() method:")
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

// unshift()
// It adds one or more elements at the BEGINNING of an array and it returns the updated array's length

/*
SYNTAX:
	arrayName.unshift('elemetA');
	arrayName.unshift('elemetA', 'elementB');
*/

console.log("Current Array:");
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

fruitsLength = fruits.unshift('Lime', 'Banana');

console.log(fruitsLength); // 8
console.log("Mutated array from unshift() method:");
console.log(fruits); // ['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

// shift()
// removes an element at the beginning of an array and returns the removed element

/*
SYNTAX:
	arrayName.shift();
*/

console.log("Current Array:");
console.log(fruits); // ['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

removedFruit = fruits.shift();

console.log("Removed fruit:");
console.log(removedFruit); // Lime
console.log("Mutated array from shift() method:");
console.log(fruits); // ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

// splice()
// simultaneously removes an element from an specified index number and adds element

/*
SYNTAX:
	arrayname.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log("Current Array:");
console.log(fruits); // ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'];

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice() method:');
console.log(fruits); // ['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

// sort()
// rearranges the array elements in alphanumeric order 
/*
SYNTAX:
	arrayname.sort()
*/

console.log("Current Array:");
console.log(fruits);

fruits.sort();

console.log('Mutated array from the sort() method:');
console.log(fruits);

// reverse()
// reverses the order of array elements
/*
SYNTAX:
	arrayname.reverse()
*/

console.log("Current Array:");
console.log(fruits);

fruits.reverse();

console.log('Mutated array from the reverse() method:');
console.log(fruits);

// [SECTION] Non-mutator methods
// Non-mutator methods are functions that do not modify or change an array after they are created

let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);

// indexOf()
// returns the index number of the first matching element found in an array
/*
SYNTAX:
	arrayName.indexOf(searchValue);
	arrayName.indexOf(searchValue, startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of firstIndex:");
console.log(firstIndex); // 1

firstIndex = countries.indexOf('PH', 2); // 5
console.log("Result of firstIndex:");
console.log(firstIndex);

// If no match found, the result will be -1
firstIndex = countries.indexOf('BR');
console.log("Result of firstIndex:");
console.log(firstIndex); // -1

// lastIndexOf()
// returns the index number of the last matching element found in an array

/*
SYNTAX:
	arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, startingIndex);
*/
// countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndex:");
console.log(lastIndex); // 5

// slice()
// portions/slices elements from an array and return a new array

/*
SYNTAX:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex endingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log("Result fron slice() method:");
console.log(slicedArrayA); // ['CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

// The elements that will be sliced are elements from starting index until the element BEFORE the ending index
let slicedArrayB = countries.slice(2, 4);
console.log("Result fron slice() method:");
console.log(slicedArrayB); // ['CA', 'SG'];

// toString()
// return an array as string separated by commas
/*
SYNTAX:
	arrayName.toString();
*/

// countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

let stringArray = countries.toString();
console.log('Result from toString method:')
console.log(stringArray);
console.log(typeof stringArray);

// [SECTION] Iteration Methods
// loops designed to perform repetitive task
// loops over all items in an array

// forEach()
// similar to a for loop that iterates on each array of elements
/*
SYNTAX:
	arrayName.forEach(function(indivElement) {
		statement
	})
*/

// countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];
countries.forEach(function(country) {
	console.log(country);
});

// map()
// iterates on each element and returns new array with different values depending on the result of the function's operation
/*
SYNTAX:
	let/const resultArray = arrayname.map(function(indivElement) {
		statement
	})
*/
let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
		return number * number;
})
console.log("Original Array:");
console.log(numbers);
console.log("Result of map() method:");
console.log(numberMap);

// filter()
// returns new array that contains elements which meets the given condition

/*
SYNTAX:
	let/const resultArray = arrayname.filter(function(indivElement) {
		statement
	})
*/
// numbers = [1, 2, 3, 4, 5]
let filterValid = numbers.filter(function(number) {
		return (number < 3);
	})
console.log("Result of filter() method:");
console.log(filterValid); // 1, 2

// includes()
// checks if the argument passed can be found in the array
/*
SYNTAX:
	arrayName.includes(<argumentToFind>);
*/

let product = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound1 = product.includes("Mouse");
console.log("Result of includes() method:");
console.log(productFound1); // true

let productFound2 = product.includes("Headset");
console.log("Result of includes() method:");
console.log(productFound2); // false

