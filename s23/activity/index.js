// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals



// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Invoke the tackle method and target a different object

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pickachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function() {
		console.log("Pickachu! I choose you!")
	}
}


console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);
console.log("Result of talk method:");
trainer.talk();

function Pokemon (name, level) {

	// properties
	this.name = name;
	this.level = level; 
	this.health = 2 * level; 
	this.attack = level;

	// method
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		// let newhealth = target.health - this.attack;
		console.log(target.name + " health is now reduced to " + Number(target.health -= this.attack)); 

		if (target.health <= 0) {
			target.faint();
		} 
	};
	this.faint = function() {
		console.log(this.name + ' fainted.');
		

	};



}

// Create/instantiate a new pokemon
let pickachu = new Pokemon('Pickachu', 12);
console.log(pickachu);

// Create/instantiate a new pokemon
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);

// Create/instantiate a new pokemon
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke the tackle method and target a different object
geodude.tackle(pickachu);

// Initialize pokemon
console.log(pickachu);

// Invoke the tackle method and target a different object
mewtwo.tackle(geodude);

// Initialize pokemon
console.log(geodude);





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}