// [SECTION] Syntax, Statements and Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
There are two types of comments:
	1. Single-line Comment - denoted by two slashes
	2. Multi-line - denoted by a slash and asterisk
*/

// Statements
// Statements in programming are instructions that we tell the comp to perform

console.log("Hello World!");

// Whitespace
// Whitespace (basically, spaces and line breaks) can impact functionality in many computer languages but NOT in JS.
console. log ("Hello World!") ;

console.

log

(
 "Hello World!"
);

// Syntax
// In programming, it is the set of rules that describes how statements must be constructed.


// [SECTION] Variable
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"

// Declaring variables - tells our devices that a variable name is created and is ready to store data
/*
SYNTAX:
	let/const variableName;
*/
let myVariables;
console.log(myVariables);

// console.log(hello); 
// let hello;

// result: Uncaught ReferenceError: Cannot access 'hello' before initialization
let hello; 

/*
Naming Convention
Camel Case: Words are joined together without spaces, and each word, except the first one, starts with a capital letter. For example, myVariableName.

Pascal Case: Similar to camel case, but the first letter of each word is capitalized. For example, MyVariableName.

Kebab Case: Words are joined together with hyphens (-) and all letters are lowercase. For example, my-variable-name.

Snake Case: Words are written in all lowercase letters and separated by underscores (_). For examample, my_variable_name.

*/

// Declaring and Initializing Variables 
// Initializing variable - the instance when a variable is given its initial value/starting value
/*
SYNTAX:
	// let/const variableName = value;
*/
let productName = "desktop computer";
console.log(productName); 
// result: desktop computer

let productPrice = 18999;
console.log(productPrice);
// result: 18999

const interest = 3.539;
console.log(interest)

// Reassigning variable values
// Reassigning variables means changing its initial value or previous value into another value
/*
SYNTAX:
	variableName = newValue
*/
productName = "Laptop";
console.log(productName); 
// result: Laptop instead of desktop computer


// let variable cannot be re-declared within its scope
// let friend = "Kate";
// let friend = "Jane";
// result : Uncaught SyntaxError: Identifier 'friend' has already been declared

// const variable cannot be updated or re assigned
// const interest = 3.539;
// interest = 4.489;
// console.log(interest);
// result : Uncaught TypeError: Assignment to constant variable.

// Reassignining variable vs. Initializing variables
// Declaration
let supplier;

// Initialization
supplier = "John Smith Tradings";
console.log(supplier);
// result : John Smith Tradings

// Reassignment
supplier = "Suitt Store"
console.log(supplier);
// result : Suitt Store

// var - is also for declaring a variable
// Hoisting is JS default behavior of moving initialization to top

a = 5;
console.log(a);
var a;

// a = 5;
// console.log(a);
// let a;
// result : Uncaught ReferenceError: Cannot access 'a' before initialization

// let/const local/global scope
// Scope essentially means where these variables are available for use
// let and const are blocked scoped
// A block is a chunk of code bounded by {}

let outerVariable = "hello"
{
	let innerVariable = "hello again";
	console.log(innerVariable);
}

console.log(outerVariable);
// console.log(innerVariable);
// result: Uncaught ReferenceError: innerVariable is not defined

// Multiple variable declaration
let productCode ='DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
// const let = "hello"
// console.log(let);
// result: Uncaught SyntaxError: let is disallowed as a lexically bound name

// [SECTION] Data types
// Strings are series of characters that create a word, phrase, a sentence or anything related to creating text
// Strings is JS can be written using either a single ('') or ("") quote
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character (\)
// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message); 
//result : John's employees went home early

// message = 'John's employees went home early';
// console.log(message);
// // result : Uncaught SyntaxError: Unexpected identifier 's'

message = 'John\'s employees went home early';
console.log(message);

message = '\\';
console.log(message);

// Number
// Integer/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);
// result : ung 2 ung base, and 10 is exponent

// Combiing number date type and strings
console.log("John's grade last quarter is " + grade);

// Boolean values are normally used to store values relating to the state of certain things
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays 
// Arrays are special kind of object data type that's used to store multiple values 

/*
SYNTAX:
	let/const arrayName = [elementA, elementB, elementC ;]
*/

// similar data types
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types (NOT RECOMMENDED)
let details = ["John", "Smith", 32, true]
console.log(details);

// Objects is a special kind of data that's used to mimic real worls object/items

/*
SYNTAX:
	let/const objectName = {
		propertyA; value,
		propertyB: value,
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}
console.log(person);

console.log(typeof grade);
console.log(typeof grades);
console.log(typeof person);
console.log(typeof myVariables);

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization
let spouse = null;
console.log(spouse);


// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName;
console.log(fullName);