// console.log("Hello World!");

// [SECTION] Arithmetic Operators
// Arithmetic Operators allow mathematical operations

let x = 4;
let y = 12;

let sum = x + y;
console.log("Result of addition operator: " + sum); // result: 16

let difference = x - y;
console.log("Result of subtraction operator: " + difference); // result: -8

let product = x * y;
console.log("Result of multiplication operator: " + product); // result: 48

let quotient = x / y;
console.log("Result of division operator: " + quotient); // result: 0.333

let remainder = x % y;
console.log("Result of modulo operator: " + remainder); // result: 4

// [SECTION] Assignment Operators
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the right operand to a variable

let assignmentNumber = 8;
console.log(assignmentNumber); // 8

// Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

// assignmentNumber += 2
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber); // result: 10

assignmentNumber += 2
console.log("Result of addition assignment operator: " + assignmentNumber); // result: 12

// Multiple Operators and Parenthesis
/*
MDAS
M - Multiplication 3 * 4 = 12
D - Division 12 / 5 = 2.4
A - Addition 1 + 2 = 3
S - Subtraction 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS operation " + mdas); // result: 0.6000000000000001


/*
PEMDAS
P - Parenthesis
E - Exponent // 2e10
M - Multiplication 
D - Division 
A - Addition 
S - Subtraction 

1. 4 / 5 = 0.8
2. 2 - 3 = -1
3. -1 * 0.8 = -0.8
4. 1 + -.08 = 0.2 or 0.1999999

*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas); // result: 0.19999999999999996


// Increment (++) and Decrement (--)
//  Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = z++;
console.log("Result of increment: " + z); // result: 2

let decrement = z--;
console.log("Result of decrement: " + z); //result: 1

// [SECTION] Type Coersion
// Type coercion is the automatic or implicit conversion of values from one data to another
// Values are automatically converted from one data type to another in order to resolve operations

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); // 1012
console.log(typeof coercion); // string

// [SECTION] Comparison Operators
// Equality Operator (==)
// Checks whether the operends are equal/have teh same content
console.log("Equality Operator:");
console.log(1 == 1); // true
console.log(1 == '1'); // true
console.log("juan" == 'juan'); // true
console.log(0 == false); // 0 = false ---> true
// 0=false 1=true

//Strict Equality Operator (===)
// Checks whether the operands are equal/have the same content and also compares the data types of the two values
console.log("Strict Equality Operator:");
console.log(1 === 1); // true
console.log(1 === '1'); // false
console.log("juan" === 'juan'); // true
console.log(0 === false); // false

// Inequality Operator
// Checks whether the operands are NOT equal/have different content
console.log("Inequality Operator:");
console.log(1 != 1); // false
console.log(1 != '1'); // false
console.log("juan" != 'juan'); // false
console.log(0 != false); // false

// Strict Inequality Operator
// Checks whether the operands are NOT equal/have different content and also compares that data types of the two values
console.log("Strict Equality Operator:");
console.log(1 !== 1); // false
console.log(1 !== '1'); // true
console.log("juan" !== 'juan'); // false
console.log(0 !== false); // true


// [SECTION] Relational Operations
let a = 50;
let b = 65;

// GT or Greater Than Operator ( > )
let isGreaterThan = a > b; // false

// LT or Less Than Operator ( < )
let isLessThan = a < b; // true

// GTE or Greater Than or Equal Operator ( >= )
let isGTorGTE = a >= b; // false

// LTE or Less Than or Equal Operator ( <= )
let isLTorLTE = a <= b; //true

console.log("Relational Operations:");
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorGTE);
console.log(isLTorLTE);

// [SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
// Returns TRUE if all operends are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet); // result: false

// Logical OR Operator (||)
// Returns TRUE if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet); // result: true

// Logical NOT Operator (||)
// Returns opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet); // result: true