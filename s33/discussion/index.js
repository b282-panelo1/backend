// console.log("Hello World!");

// [SECTION] JavaScript Sychronous and Asynchronous
// JS is by default synchronous, meaning that only one statemnet us executed at a time
console.log("Hello World!");
console.log("Hello World Again!");

// Asynchronous means that we can proceed to execute other statements, while consuming is running in the background

// [SECTION] Getting all posts
// The fetch API allows you to asynchronously request for a resource (data)
// "Promise" is an object that represents the eventual completion or failure of an asynchronous function and its resulting value

/*
SYNTAX:
	fetch('URL')
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));

// Fetch and .then
/*
SYNTAX:
	fetch('URL')
	.tehn(response)=> {})
*/

// "fetch" method will return a "promise" that resolves to a "response object" 

fetch('https://jsonplaceholder.typicode.com/posts/')
// ".then" method captures the "response object" and return another promise which will be eventually be resolved or rejected
.then(response => console.log(response));

fetch('https://jsonplaceholder.typicode.com/posts/')
// ".json" method from the response object to convert the data retrieved into JSON method format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from "fetch request"
.then((json) => console.table(json));


// Async and Await

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

/*
async function fetchData() {

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');

	// Result returned by fetch is a returns a promise
	console.log(result);

	// The returned "Response" is an object
	console.log(typeof result);

	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json()
	// Print out the content of the "Response" object
	console.log(json);

};
fetchData();
*/


// [SECTION] Creating a post

fetch('https://jsonplaceholder.typicode.com/posts/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	    userId: 1,
	    title: "Create",
	    body: "Body of post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Updating a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	    userId: 1,
	    title: "Updated post",
	    body: "Hello again"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Delete a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})



