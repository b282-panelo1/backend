// Use the 'require' directive to load the express module/package
const express = require("express");

// Create an application using express
// "app" is our server
const app = express();

const port = 3000;

// Methods used from express.js are middleware
// Middleware is software that provides services and capabilities to application outside of what's offeres by the operating sysmtem

// allows your app to read json data
// convert to json format
app.use(express.json());

// allows your app to read data from any forms
// pang read pa ng ibang forms
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// GET
app.get("/greet", (request, response) => {
	// "response.send" method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
});

// POST
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple Registration

let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password!")
	}
})


// Activity

// GET /home
app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
});

// GET /users




app.get("/users", (request, response) => {
	response.send(
		users = [
			{
				"username": "johndoe",
				"password": "johndoe1234"
			}
		])
	
});

// DELETE /delete-user


users = [
  { id: 1, name: 'johndoe' },
];


app.delete('/delete-user', (request, response) => {
  const { username } = request.body;
  let message;

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (users[i].name === username) {
        const deletedUser = users.splice(i, 1)[0];
        message = `User ${deletedUser.name} has been deleted.`;
        break;
      }
    }

    if (message === undefined) {
      message = 'User does not exist.';
    }
  } else {
    message = 'No users found.';
  }

  response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
